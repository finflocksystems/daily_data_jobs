import asyncio
import datetime
import hashlib
import json
import pickle
## Importing Libraries
import sys
import time
import warnings
from copy import deepcopy
from datetime import date, timedelta

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import quandl
import redis
import requests
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure, output_file, show
from bson import json_util
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from flask_cors import CORS
from ib_insync import *
from nsepy import get_history
from scipy.stats import zscore
from sklearn import linear_model, preprocessing
from sklearn.decomposition import PCA, FastICA, TruncatedSVD
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.metrics import (accuracy_score, confusion_matrix, f1_score,
                             precision_score, recall_score)
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import BernoulliRBM
from sklearn.random_projection import (GaussianRandomProjection,
                                       SparseRandomProjection)
from statsmodels.graphics.tsaplots import plot_pacf
from ta import *

import pyodbc
from xgboost import XGBClassifier
from nsetools import Nse
import time
import datetime
from copy import deepcopy
import time
import sqlite3
from pandas.io import sql

DATABASE_NAME = 'Daily_Data.db'
cnx = sqlite3.connect(DATABASE_NAME)

## Ignoring Warnings
import warnings
warnings.filterwarnings("ignore")




## Indian Technical Data
df = pd.DataFrame()
Final_df = pd.DataFrame()
b = pd.DataFrame()
df_names = pd.read_csv('NSE_Symbols.csv')
print(df_names.columns)
name = list(df_names['Symbol'])
for number , n in enumerate(name):
    try:

        df = pd.DataFrame()
        d1 = date.today()
        d2 = date.today() - timedelta(days=55)
        df = get_history(symbol=n, start=d2, end=d1)
        df['Timestamp'] = df.index
        df['Symbol'] = n
        df = df[['Symbol', 'Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)

        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg','trend_adx_ind', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']
        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal','ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']
        t1 = time.time()
        df = df[["Symbol", 'Timestamp' , 'trend_macd_signal', 'trend_ema_fast','trend_ema_slow', 'trend_adx_pos','trend_vortex_ind_pos','trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst','trend_ichimoku_a', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch', 'momentum_wr', 'momentum_ao']]
        df.columns = ['Symbol', 'Timestamp' , 'MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']
        Final_df = Final_df.append(df)
        print("Completed : " , number)
    except Exception as e:
        print("Error : " , e)
        

maxt = max(Final_df['Timestamp'])
Final_df = Final_df.loc[df['Timestamp'] == maxt]
Final_df = Final_df[['Symbol' ,'Timestamp', 'MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']]
b = Final_df
df = b
a1 = df['Symbol']
a2 = df['Timestamp']
df.drop(['Symbol' , 'Timestamp'] , inplace=True , axis=1)
df = df.fillna(0)
x = df.values 
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)

df = pd.DataFrame(x_scaled)
df[df >= 0.8] = 2
df[(df >0.2) & (df <0.8)] = 1
df[df <= 0.2] = 0
df[df == 0] = "BUY"
df[df == 2] = "SELL"
df[df == 1] = "HOLD"
df.columns = ['MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']
df['Symbol'] = list(a1)
df['Timestamp'] = list(a2)
df.to_sql('Indian_Technicals', con=cnx , if_exists='replace')





## American Technical Data
df = pd.DataFrame()
Final_df = pd.DataFrame()
b = pd.DataFrame()
quandl.ApiConfig.api_key = 'oLBXk1amsgvrxPDkpEyU'
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
cursor = cnxn.cursor()
query = "select Symbol from American_Stocks_Sectors"
a = pd.read_sql_query(query , cnxn)
name = list(a['Symbol'])


for n in name:
    try:

        df = pd.DataFrame()
        d1 = date.today()
        d2 = date.today() - timedelta(days=55)
        df = quandl.get('EOD/' + str(n), start_date=d2, end_date=d1)
        df['Timestamp'] = df.index
        df['Symbol'] = n
        df = df[['Symbol', 'Timestamp' , 'Open' , 'High' , 'Low' , 'Close' , 'Volume']]
        df.reset_index(drop=True, inplace=True)
        df = add_all_ta_features(df, "Open", "High", "Low", "Close", "Volume", fillna=True)

        Indicators_List = ['trend_macd_signal', 'trend_macd_diff', 'trend_ema_fast','trend_ema_slow', 'trend_adx', 'trend_adx_pos', 'trend_adx_neg','trend_adx_ind', 'trend_vortex_ind_pos', 'trend_vortex_ind_neg','trend_vortex_diff', 'trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst', 'trend_kst_sig', 'trend_kst_diff','trend_ichimoku_a', 'trend_ichimoku_b', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch','momentum_stoch_signal', 'momentum_wr', 'momentum_ao']
        Signal_List = ['MACD_Signal', 'MACD_Diff_Signal', 'EMA_Fast_Signal','EMA_Slow_Signal', 'ADX_Signal', 'ADX_Pos_Signal', 'ADX_Neg_Signal','ADX_Ind_Signal', 'Vortex_Pos_Signal', 'Vortex_Neg_Signal','Vortex_Diff_Signal', 'Trix_Signal', 'Mass_Signal', 'CCI_Signal','DPO_Signal', 'KST_Signal', 'KST_Sig_Signal', 'KST_Diff_Signal','ICHIMOKU_Signal', 'ICHIMOKU_B_Signal', 'RSI_Signal', 'MFI_Signal','TSI_Signal', 'UO_Signal', 'Stoch_Signal','Stoch_Sig_Signal', 'WR_Signal', 'AO_Signal']
        t1 = time.time()
        df = df[["Symbol", 'Timestamp' , 'trend_macd_signal', 'trend_ema_fast','trend_ema_slow', 'trend_adx_pos','trend_vortex_ind_pos','trend_trix', 'trend_mass_index', 'trend_cci','trend_dpo', 'trend_kst','trend_ichimoku_a', 'momentum_rsi', 'momentum_mfi','momentum_tsi', 'momentum_uo', 'momentum_stoch', 'momentum_wr', 'momentum_ao']]
        df.columns = ['Symbol', 'Timestamp' , 'MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']
        Final_df = Final_df.append(df)
    except Exception as e:
        print("Error : " , e)
        

maxt = max(Final_df['Timestamp'])
Final_df = Final_df.loc[df['Timestamp'] == maxt]
Final_df = Final_df[['Symbol' ,'Timestamp', 'MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']]
b = Final_df
df = b
a1 = df['Symbol']
a2 = df['Timestamp']
df.drop(['Symbol' , 'Timestamp'] , inplace=True , axis=1)
df = df.fillna(0)
x = df.values 
min_max_scaler = preprocessing.MinMaxScaler()
x_scaled = min_max_scaler.fit_transform(x)
df = pd.DataFrame(x_scaled)
df[df >= 0.8] = 2
df[(df >0.2) & (df <0.8)] = 1
df[df <= 0.2] = 0
df[df == 0] = "BUY"
df[df == 2] = "SELL"
df[df == 1] = "HOLD"
df.columns = ['MACD', 'EMA','EMAS', 'ADX','Vortex','Trix', 'Mass', 'CCI','DPO', 'KST','ICHIMOKU', 'RSI', 'MFI','TSI', 'UO','Stoch', 'WR', 'AO']
df['Symbol'] = list(a1)
df['Timestamp'] = list(a2)
df.to_sql('American_Technicals', con=cnx , if_exists='replace')












## American Fundamental Data
ALL_DF_FS = pd.read_csv('American_Financial_Statements.csv')
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
cursor = cnxn.cursor()
query = "select Symbol from American_Stocks_Sectors"
a = pd.read_sql_query(query , cnxn)
a = list(a['Symbol'])
a = a[0:200]

b = 'American'
c = ['ATOT' , 'SREV'  , 'NINC' , 'SGRP']
File_names = ['American_FundamentalsATOT' , 'American_FundamentalsSREV' , 'American_FundamentalsLLTD' , 'American_FundamentalsNINC' , 'American_FundamentalsSGRP']

for ration , ratio in enumerate(c):
    x = []
    Final_DF = pd.DataFrame()
    df = pd.DataFrame()

    for i in a:
        try:
            df = ALL_DF_FS.loc[(ALL_DF_FS['Company_Symbol'] == i) & (ALL_DF_FS['Abbreviation'] == ratio)& (ALL_DF_FS['Fiscal_Period_Type'] == 'Annual')]
            df = df.sort_values(['End_Date'], ascending=True)
            Final_DF[i] =  list(df['Abbreviation_Value'])
            x.append(i)
        except Exception as e:
            print("ERROR" , e)
        

    try:
        Final_DF['Date'] = list(df['End_Date'])
    except:
        pass
    columns = ['Date']
    columns.extend(x)
    print(Final_DF)
    Final_DF = Final_DF[columns]
    Final_DF.index = Final_DF['Date']
    Final_DF.drop('Date' , axis=1 , inplace=True)
    b = Final_DF.T
    b['Symbol'] = b.index
    print(b)
    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=tcp:finflock.database.windows.net;DATABASE=MARKETDATA;UID=system.services@finflock.com;PWD=Finflock123!')
    cursor = cnxn.cursor()
    query = "select * from American_Stocks_Sectors"
    s = pd.read_sql_query(query , cnxn)
    c = pd.merge(b, s, how='inner')
    c['Prediction'] = 0
    for i in range( 0 , c.shape[0]):
        try:
            model = LinearRegression()
            model.fit(pd.DataFrame([1,2,3,4,5,6]), pd.DataFrame([c.iloc[i][0] , c.iloc[i][1] , c.iloc[i][2] , c.iloc[i][3] , c.iloc[i][4] , c.iloc[i][5] ]))
            c['Prediction'][i] = model.predict(7)[0][0]
        except:
            c['Prediction'][i] = 'NAN'
    b = c
    b.columns = ['Dec 2012' , 'Dec 2013' , 'Dec 2014' , 'Dec 2015' , 'Dec 2016' , 'Dec 2017' , 'Symbol' , 'Name' , 'MarketCap' , 'Sector' , 'Prediction']

    b['Signal'] = 0
    for i in range( 0 , b.shape[0]):
        try:
            if b['Prediction'][i] - float(b['Dec 2017'][i])  > 0:
                print("BUY")
                b['Signal'][i] = "BUY"
            else:
                print("SELL")
                b['Signal'][i] = "SELL"
        except:
            b['Signal'][i] = "NAN"

    b = b.drop_duplicates(subset=['Symbol'])
    b.to_sql(File_names[ration], con=cnx , if_exists='replace')
    print(File_names[ration])
    print("Database WRITTEN....")
