## SQLITE
import pandas as pd
import sqlite3
from pandas.io import sql
 
## Create dummy dataframe
a = [[1,2,3,4,5] , [5,6,7,8,9]]
df = pd.DataFrame(a)
df.columns = ['A' , 'B' , 'C' , 'D', 'E']
 
## Write in SQlite
cnx = sqlite3.connect('Daily_Data.db')
df.to_sql('Table', con=cnx , if_exists='append')
 
## Read from SQLite
df2 = pd.read_sql('select * from Table, con=cnx)
 